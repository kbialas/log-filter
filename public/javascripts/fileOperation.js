var express = require('express');
var fs = require('fs');
var path = require("path");
var filename = "log.txt";
var filePath = path.resolve(path.join(__dirname, "", filename));

var fileOperation = function() {
    var fileArray = fs.readFileSync(filePath).toString().split("\n");
    var finalArray = [];
    fileArray.forEach(function(item, index, object){
        if (item.indexOf('fbcampaign') !== -1) {
            finalArray.push(fileArray[index]);
        }
    });
    return finalArray;
};

module.exports = fileOperation;