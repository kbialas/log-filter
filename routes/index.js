var express = require('express');
var router = express.Router();

/* GET home page. */
var fileOperation = require('../public/javascripts/fileOperation');
var endArray = fileOperation();

router.get('/', function(req, res, next) {
  res.render('index', {
      title: 'Log',
      message: endArray
  });
});

module.exports = router;
